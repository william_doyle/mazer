/*  mazer
    Copyright (C) 2020  William Doyle

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "stack.h"

// William Doyle
// Febuary 17th 2020

// constructors
struct lnode * make_lnode(void * __data){
	struct lnode * self = malloc(sizeof(struct lnode));
	if (!self){
		fprintf(stderr, "Malloc failed in function %s\n", __func__);
		abort();
	}
	self->next = NULL;
	self->data = __data;
}

void destroy_lnode(struct lnode * self){
	if (self){
		free(self);
	} else {
		fprintf(stderr, "Nothing to destroy in function %s\n", __func__);
	}
}

void __push(struct stack * self, void * __data){
	struct lnode * new_node = make_lnode(__data); 
	struct lnode * node_swap_container;
	node_swap_container = self->top;
	self->top = new_node;
	new_node->next = node_swap_container;
}

void* __pop (struct stack *self){
	void * return_value = NULL; 
	if (self->top != NULL){
		return_value = self->top->data;
		struct lnode * temp = self->top->next;
		free(self->top);
		self->top = temp;
	}
	return return_value;	
}

struct stack * make_stack(void){
	struct stack * self = malloc(sizeof(struct stack));
	self->pop = __pop;
	self->push = __push;
	self->top = NULL;
	return self;
}

void destroy_stack(struct stack * self){
	if (self != NULL){
		while(self->top != NULL){
			self->pop(self);
		}
		free(self);
	}
}





