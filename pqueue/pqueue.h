/*  mazer
    Copyright (C) 2020  William Doyle

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#pragma once
#include <stdlib.h>
#include <stdio.h>

// simple node based priority queue for storing void pointers
// william doyle
// Febuary 13th 2020

struct pq_node{
	void * data;
	struct pq_node * next;
};

struct pqueue{
	void*(*dequeue)(struct pqueue *);
	void(*enqueue)(struct pqueue *, void*);
	int (*compare)(void*, void*);
	void (*print_raw)(struct pqueue *);
	struct pq_node * top;
};

void destroy_pqueue(struct pqueue *);
struct pq_node * make_pq_node(void *);
struct pqueue * make_pqueue(int(*)(void*, void*));

void * __dequeue(struct pqueue * );
void __enqueue(struct pqueue * , void *);
void __print_raw(struct pqueue *);
int __int_compare(void *, void *);

//int __float_compare(void *, void *) __attribute__((depricated("__float_compare is unstable and should not be used!"))); // don't use -> highly unstable
int __short_compare(void *, void *);
int __long_compare(void *, void *);
int __unsigned_compare(void *, void *);
int __double_compare(void *, void *);
int __string_compare(void *, void *);

