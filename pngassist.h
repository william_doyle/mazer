/*  mazer
    Copyright (C) 2020  William Doyle

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/
#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <libpng/png.h>

//adapted from https://gist.github.com/1995eaton/1fd160fa776f31044b40
//note that a png_byte is defined as an unsigned char

//	line 449	https://github.com/LuaDist/libpng/blob/master/pngconf.h

typedef struct PNG {
  int width;
  int height;
  int depth;
  int pixel_bytes;
  png_bytep color;
  png_structp pngp;
  png_byte color_type;
  png_infop infop;
  png_byte **data;
} PNG;

int put_pixel(PNG *png, int x, int y) ;

void write_png(PNG *png, char *fname) ;

void set_color(PNG *png, png_byte r, png_byte g, png_byte b);

int read_png(PNG *png, const char *fname);

void create_png(PNG *png, int width, int height, png_byte color_mode);

void draw_rect(PNG *png, int x, int y, int width, int height);

void clear_png(PNG*);
