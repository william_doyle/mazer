/*  mazer
    Copyright (C) 2020  William Doyle

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


#pragma once
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

/*
 	guide:
	call check_malloc(pointer_you_want_to_check, __func__);
*/
static inline int check_malloc(void * value, const char * funk){
	if (value){
		return 1;
	}else {	/* malloc failed */
		time_t now;
		time(&now);
		FILE * fp = fopen("MALLOC_LOG.txt", "a");
		fprintf(fp, "Malloc failed! Function: %s Time: %s\n", funk, ctime(&now));
		fclose(fp);
	}
	return 0;
}
