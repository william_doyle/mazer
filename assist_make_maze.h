/*  mazer
    Copyright (C) 2020  William Doyle

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


#pragma once
#include "maze.h"

int * no_go_h;
int * no_go_v;
int index_h;
int index_v;
/*
	used by make_maze.c
	a bunch of inline functions to assist the make_maze function
*/
/* fill edges with edge values */
static inline void fill_edges(int * marr){
	for (int i = 0; i < MAZE_SIZE; i++){
		for (int k = 0; k < MAZE_SIZE; k++){
			if ( (i == 0) || (k == 0) || (i == MAZE_SIZE-1) || (k == MAZE_SIZE-1 )){
				*(marr+i*MAZE_SIZE+k) = 1;
			}// close if
		}// close for k
	}// close for i
}

/* clear the maze by setting all cells to 0 (aka false ) */
static inline void clear_maze(int * marr){
	for (int i = 0; i < MAZE_SIZE; i++){
		for (int k = 0; k < MAZE_SIZE; k++){
			*(marr+i*MAZE_SIZE+k) = 0;
		}
	}
}

/* create opening at north wall and south wall --- start and finish positions */
static inline void open_start_finish(int * marr, int start, int finish){
	*(marr+start) = 0;				//set start opening
	*(marr+(MAZE_SIZE*(MAZE_SIZE-1))+finish) = 0;	//start finish opening
}



static inline int check_if_conflict(int level, int wall){
	extern int * no_go_h;
	extern int * no_go_v;
	
	extern int index_h;
	extern int index_v;
	int limit = (level)? index_h: index_v;

	for (int i = 0; i < limit; i++){
		if(level){
			if (no_go_h[i] == wall)
				return 1;		// 1 as in true as in YES THIS VALUE IS IN CONFLICT AND YOU NEED A NEW ONE
		}else{
			if (no_go_v[i] == wall)
				return 1;

		}
	}	
	
	/*temp*/return 0;

}

static inline int division_by_wall (int * wall,  int i_start, int k_start,
	       	int i_finish, int k_finish, int ** chamber, int * door, int level){
	extern int * no_go_h;
	extern int * no_go_v;

	extern int index_h;
	extern int index_v;
	
	int ticks_till_fuck_it = 30; // we can't be recursive all day
	do{
		if (ticks_till_fuck_it-- <= 0) return -1;
		if (level){
			*wall = (rand()%(i_finish - i_start +1))+ i_start;	// random x wall location
			*door = (rand()%(k_finish - k_start +1))+ k_start;	// random x wall location
		}else{
			*wall = (rand()%(k_finish - k_start +1))+ k_start;	// random x wall location
			*door = (rand()%(i_finish - i_start +1))+ i_start;	// random x wall location
		}
	}while(check_if_conflict(level, *wall) );

	for (int i = i_start; i < i_finish; i++)
			for (int k = k_start; k < k_finish; k++)
				if (level){
					if ((i == *wall)&&(k != *door))
						chamber[i][k] = 1;
				}else {
					if ((k == *wall)&&(i != *door))
						chamber[i][k] = 1;
				}
	return level; 
}


static inline int put_hole(int wall, int level, int start, int end, int ** chamber){
	int door = rand()%(end - start +1) + start;
	if (level)
		chamber[wall][door] = 0;
	else
		chamber[door][wall] = 0;
	return door;

}
