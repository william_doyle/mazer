/*  mazer
    Copyright (C) 2020  William Doyle

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "astar.h"


#ifdef SLOW_DIJ
	#ifndef SLOW_MODE
		#define SLOW_MODE
	#endif
#endif

bool astar_quit_slow_mode = false;
void * astar_slow_show( void * data){
#ifdef SLOW_MODE
	int ** maze = (int ** )data;
	for (;;){
//		usleep(100);
		fast_show_maze(maze);
		if (astar_quit_slow_mode == true){
			return NULL;
		}
	}
#endif
}

int compare_astar_nodes(void * n1, void * n2){
	struct astar_node * dnode1 = (struct astar_node *)n1;
	struct astar_node * dnode2 = (struct astar_node *)n2;
	if (dnode1->tent_score == dnode2->tent_score){
		return 0;
	} else if (dnode1->tent_score > dnode2->tent_score){
		return 1;
	} else if (dnode1->tent_score < dnode2->tent_score){
		return -1;
	}
	printf("Unexpected result in %s!\n", __func__);
	return 100;
}

int astar(int ** maze, const struct pos start, const struct pos end, int * marr){
	struct pqueue * _nodes = make_pqueue(compare_astar_nodes);												
	struct astar_node * current;
	struct astar_node * start_node = make_astar_node(start.x, start.y, false, 0, NULL);
	struct astar_node * end_node = make_astar_node(end.x, end.y, false, _INF, NULL);
	struct stack * unvisited_set = make_stack();
	maze[start_node->x][start_node->y] = 3;
	maze[end_node->x][end_node->y] = 3;
	
#ifdef SLOW_DIJ
	pthread_t ptid;
	pthread_create(&ptid, NULL, astar_slow_show, (void*)maze);
#endif
	const int __amount__ = 4000;//15000;

	_nodes->enqueue(_nodes, (void*)end_node);	
	astar_create_nodes(MAZE_SIZE, maze, _nodes);																	// create nodes at maze junctions
	astar_prepare_and_display_nodes(_nodes, start_node, maze, unvisited_set );									// prepare nodes and set them on maze
	start_node->tent_score = 0;																				// set initial node tent_score to zero

	// set the initial node as current
	for (current = start_node; current != NULL; current = get_node_with_smallest_tent_score(unvisited_set)){
		// consider all of its unvisited neighbours
		struct stack * uvis_nebor = make_stack();
		struct astar_node * best_north = NULL;
		struct astar_node * best_east = NULL;
		struct astar_node * best_south = NULL;
		struct astar_node * best_west = NULL;

		if (current->tent_score == INF){
			break;
		}
#ifdef SLOW_MODE
		maze[current->x][current->y] = 4;
		usleep(__amount__);
#endif
		for (struct pq_node* i = _nodes->top; i != NULL; i = i->next){
			// if data is a perfect best neighbour push it to the stack
			if ((*(struct astar_node *)i->data).x == current->x){			// if x's are the same
				if ((*(struct astar_node *)i->data).y > current->y ){		// if is higher north than me
					if	(is_better_neighbour_than(current, best_north, i, maze, 'N'))
						best_north = ((struct astar_node *)i->data);
				}
				else if ((*(struct astar_node *)i->data).y < current->y ){		// if is further south than me
					if (is_better_neighbour_than(current, best_south, i, maze, 'S'))	
						best_south = (struct astar_node *)(i->data);
				}
			}
			else if ((*(struct astar_node *)i->data).y == current->y)	{	// if y's are the same
				if ((*(struct astar_node *)i->data).x > current->x ){		// if is higher east than me
					if (is_better_neighbour_than(current, best_east, i, maze, 'E'))	
						best_east = (struct astar_node *)(i->data);
				}
				else if ((*(struct astar_node *)i->data).x < current->x ){		// if is further west than me
					if (is_better_neighbour_than(current, best_west, i, maze, 'W'))		
						best_west = (struct astar_node *)(i->data);
				}
			}
		}
		astar_group_push(best_north, best_east, best_south, best_west, uvis_nebor);

		struct astar_node * cur_nei = (struct astar_node*)uvis_nebor->pop(uvis_nebor);
		while (cur_nei != NULL){
			// calculate cur_nei tent_score through curent node
			long tent_score_through_current;
			tent_score_through_current = current->tent_score + astar_get_displacment(current, cur_nei) - closer_to_goal_bonus(current, cur_nei, end_node); 
#ifdef SLOW_MODE
			maze[cur_nei->x][cur_nei->y] = 5;
			usleep(__amount__);
#endif
			// issue also here
			if (tent_score_through_current < cur_nei->tent_score){	// compair newly calculated tent_score to current assigned value of neighbours
				cur_nei->tent_score = tent_score_through_current;
				cur_nei->prev = current;							// assign the smaller one to cur_nei->tent_score		
			}
			else {
				// keep the same
				cur_nei->tent_score = cur_nei->tent_score;
			}
			cur_nei = (struct astar_node*)uvis_nebor->pop(uvis_nebor);
		}

		// when we are done considering all unvisited neighbours of the current node
		current->visited = true;		// mark the current node as visited
		astar_remove_node_from_set( unvisited_set, current);	// remove the current node from the set 
		destroy_stack(uvis_nebor);
		if (end_node->visited == true ){		// if the destination node has been marked visited
			break;
		}
	}	
	
	// find path from start node to end node
#ifdef SLOW_MODE
	force_missed_nodes_back_to_green(MAZE_SIZE, maze);
#endif
//	fprintf(stderr, "About to call astar_trace_path\n");
	astar_trace_best_path(current, end_node, maze, __amount__);
//	fprintf(stderr, "Back in %s\n", __func__);	
#ifdef SLOW_DIJ
	astar_quit_slow_mode = true;
	pthread_join(ptid, NULL);
#endif

	//free malloced memory
	destroy_stack(unvisited_set);
	for (struct pq_node* i = _nodes->top; i != NULL; i = i->next){
		free_astar_node((struct astar_node *)i);
	}
	free_astar_node(start_node);
	free_astar_node(end_node);

	return 1;

}
