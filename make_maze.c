/*
    mazer
    Copyright (C) 2020  William Doyle

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "maze.h"
#include "assist_make_maze.h"
#include <stdbool.h>
#include <sys/types.h>
#include <sys/wait.h>

/*
	Make a maze in a 2D array
	https://en.wikipedia.org/wiki/Maze_generation_algorithm#Recursive_division_method
*/
//	#define DEBUG


#define SHOW_MAKE_SLOW
#define RECURSIVE_CHANGE 1
#define RC RECURSIVE_CHANGE
//	#define OBVIOUS_HOLES
/* recursive maze generation method */
#define MORE_HOLES
void rmgm(int ** , int, int, int, int);

/**
 *	method name: log_reason_for_return
 * */
void log_reason_for_return(const char * filename, const char * message){
		FILE *fp = fopen(filename, "w");
		time_t now;
		time(&now);
		fprintf(fp, "MESSAGE: %s TIME: %s", message, ctime(&now));
		fclose(fp);
}

int make_maze(int * marr){
	
	extern int * no_go_h;
	extern int * no_go_v;
	extern int index_h;
	extern int index_v;
	
	no_go_h = malloc(MAZE_SIZE*sizeof(int)*2);
	no_go_v = malloc(MAZE_SIZE*sizeof(int)*2);

	no_go_v[index_v++] = ENTER;
	no_go_v[index_v++] = EXIT;

	clear_maze(marr);			// clear maze
	fill_edges(marr);			// make edges full
	open_start_finish(marr, ENTER, EXIT);	// open start pos and end pos

	/* start recursive division generation */

	int **chamber;
	chamber = malloc(sizeof(int*)*MAZE_SIZE);		// malloc the container
	for (int i = 0; i < MAZE_SIZE; i++)			// for the size of the container
		chamber[i] = malloc(sizeof(int) * MAZE_SIZE);	// malloc each sub container

	place_single_ptr_2d_array_into_dual_ptr_2d_array(marr, chamber, MAZE_SIZE);	

	rmgm(chamber, 0, 0, MAZE_SIZE, MAZE_SIZE);		// begin recursive maze creation

	place_dual_ptr_2d_array_into_single_ptr_2d_array(chamber, marr, MAZE_SIZE);

	for (int i = 0; i < MAZE_SIZE; i++)	// for every row in chamber
		if (chamber[i] != NULL)		// if the row has malloced memory
			free(chamber[i]);	// free that memory
	if (chamber != NULL)			// if chamber itself has malloced memory
		free(chamber);			// free THAT memory

#ifdef DEBUG
	show_maze(marr);
#endif
	if(no_go_h)free(no_go_h);
	if(no_go_v)free(no_go_v);
	return 1;
}



static inline bool test_vwall(int vwall, int sx, int xsize){
	return ((vwall==sx)||(vwall==sx+1)||(vwall==xsize)||(vwall==MAZE_SIZE-2)||(vwall==MAZE_SIZE-1)||(vwall==MAZE_SIZE));
}

static inline bool test_hwall(int hwall, int sy, int ysize){
	return ((hwall==sy)||(hwall==sy+1)||(hwall==ysize)||(hwall==MAZE_SIZE-2)||(hwall == MAZE_SIZE-1)||(hwall == MAZE_SIZE));
}

static inline bool test_wall(int wall, int start, int size, char ori){
	if (ori == 'v')
		return test_vwall(wall, start, size);
	else if (ori == 'h')
		return test_hwall(wall, start, size);
	else {
		printf("In function: %s variable: ori has unexpected value!\n", __func__);
		return true;
	}
}

static inline bool force_wall_even(int * wall, int start, int size, char ori){
	if (*wall%2 == 1){
		if (!test_wall(*wall-1, start, size, ori)){
			--*wall;
		}else if (!test_wall(*wall+1, start, size, ori)){
			++*wall;
		}else {
			return true;
		}
	}
	return false;
}

static inline bool force_hole_odd(int * hole, int should_be_smaller, int should_be_larger ){
	if (*hole%2 == 0){
		if (!(*hole+1 > should_be_smaller)){
			++*hole;
		}else if (!(*hole-1 < should_be_larger)){
			--*hole;
		}else {
			return true;
		}
	}
	return false;
}

static inline void random_hole(bool do_hwall, int * hole, int xsize, int ysize, int sx, int sy){
	if (do_hwall){
		*hole = rand()%((xsize-1)-(sx))+(sx);
	}else {
		*hole = rand()%((ysize-1)-(sy))+(sy);
	}
}

void rmgm(int ** chamber, int sx, int sy, int xsize, int ysize){
#define RECURSIVE_LIMIT 1
	if (xsize - sx < RECURSIVE_LIMIT)return;
	if (ysize - sy < RECURSIVE_LIMIT)return;

	bool do_vwall = true, do_hwall = true;
	int hwall, vwall;

	hwall = (ysize +1 - sy)/2 + sy;
	vwall = (xsize +1 - sx)/2 + sx;

//	#define RANDOM_WALLS	// tends to lead to large open spaces... should be worked to be better. for now leaving disabled.
#ifdef RANDOM_WALLS
	hwall = rand()%(ysize  - sy) + sy;
	vwall = rand()%(xsize  - sx) + sx;
#endif

	// make sure walls are on even spot
	if (force_wall_even(&hwall, sy, ysize, 'h'))
		return;
	if (force_wall_even(&vwall, sx, xsize, 'v'))
		return;

	do_vwall = !(test_vwall(vwall, sx, xsize));
	do_hwall = !(test_hwall(hwall, sy, ysize));
	
	if ((do_vwall == false)&&(do_hwall == false)){
#ifdef DEBUG
		log_reason_for_return("ErrorLog.txt", "both do walls are false");
#endif
		return;
	}
	//randomly disable one of the walls
	if (rand()%2 == 0){
		if (do_hwall == true)
			do_vwall = false;
	}else{ 
		if (do_vwall == true)
			do_hwall = false;
	}

	
	int hole;
	random_hole(do_hwall, &hole, xsize, ysize, sx, sy);
	int count = 0;
	do{
		if (force_hole_odd(&hole, (do_hwall)? sx:sy, (do_hwall)? xsize:sy)){
			if (count++ > 15){
#ifdef DEBUG
				log_reason_for_return("ErrorLog.txt", "force hole odd failed!");
#endif
				return;
			} else {
				random_hole(do_hwall, &hole, xsize, ysize, sx, sy);
			}
		}else {
			break;
		}
	}while(true);
	for (int i = sx; i < xsize; i++){
		for (int k = sy; k < ysize; k++){
			if (do_vwall){
				if ((i == vwall)&&(k != hole)){
					chamber[i][k] = 1;
				}
#ifdef OBVIOUS_HOLES
				else if ((i == vwall)&&(k == hole)){
						chamber[i][k] = 2;
					}
#endif
			}
			if (do_hwall){
				if ((k == hwall)&&(i != hole)){
					chamber[i][k] = 1;
				}
#ifdef OBVIOUS_HOLES
				else if ((k == hwall)&&(i == hole)){
						chamber[i][k] = 2;
					}
#endif
			}
		}//close for k
	}//close for i

#ifdef SHOW_MAKE_SLOW
	#define VERBOSE
#endif
#ifndef VERBOSE
		if (do_vwall){
			rmgm(chamber, sx, sy, vwall, ysize);
			rmgm(chamber, vwall, sy, xsize, ysize);
		}
		else if (do_hwall){
			rmgm(chamber, sx, sy, xsize, hwall); 
			rmgm(chamber, sx, hwall, xsize, ysize); 
		}
#endif
#ifdef VERBOSE
//	char wait;
//	fast_show_maze(chamber);
		if (do_vwall){
			int fid = fork();
			if (fid != 0){
				rmgm(chamber, sx, sy, vwall, ysize);
				exit(0);
			}
			rmgm(chamber, vwall, sy, xsize, ysize);
			pid_t waiter;
			waitpid(0, &waiter, 0);
		}
		else if (do_hwall){
			int fid = fork();
			if (fid != 0){
				rmgm(chamber, sx, sy, xsize, hwall); 
				exit(0);
			}
			rmgm(chamber, sx, hwall, xsize, ysize); 
			pid_t waiter;
			waitpid(0, &waiter, 0);
		}
#endif
}


