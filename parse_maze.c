 /*
	mazer
    Copyright (C) 2020  William Doyle

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "maze.h"

/*
	read a maze from a png and parse it into a 2D array
*/

//	#define DEBUG

int parse_maze(int * marr, const char * filename){

//	return 0; // disable this step

	/* read file and store it as bit map in marr */

	PNG imaze;			//imaze as in image maze
	read_png(&imaze, filename);

#define CURRENT_MAZE_CELL *(marr+i*MAZE_SIZE+(k/3))

	for (int i = 0; i < MAZE_SIZE; i++){
		for (int k = 0; k < MAZE_SIZE*3; k+=3){
			if ((imaze.data[i][k]==255)&&(imaze.data[i][k+1]==255)&&(imaze.data[i][k+2]==255)){
				*(marr+i*MAZE_SIZE+(k/3)) = 0;
			}
			else {
				*(marr+i*MAZE_SIZE+(k/3)) = 1;
			}//close else
		}//close for k
	}//close for i


#ifdef DEBUG
	char wait;
	show_maze(marr);
	scanf("%c", &wait);
#endif
				
	clear_png(&imaze);

	return 1;
}
