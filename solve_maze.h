/*  mazer
    Copyright (C) 2020  William Doyle

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#pragma once
#include "maze.h"

struct pos {
	int x, y;
};

/**
 *	name: 		is_in_bounds()
 *	author: 	William Doyle
 *	params:		3 all ints
 *	description:	
 *
 *
 * */
static inline int is_in_bounds(int test_case, int lower_bound, int upper_bound){

	if (test_case < lower_bound) {
		return 0;
	}
	if (test_case > upper_bound-1){
		return 0;
	}

	return 1; // success
}

/**
 *	name: 		
 *	author: 	William Doyle
 *	params:	
 *	description:	
 *
 **/
static inline void update_and_show_maze(int ** maze, int * marr){
		for (int i = 0; i < MAZE_SIZE; i++){
			for (int k = 0; k < MAZE_SIZE; k++){
				*(marr+i*MAZE_SIZE+k) = maze[i][k];
			}
		}
		show_maze(marr);
}

/**
 *	name: 		
 *	author: 	William Doyle
 *	params:		
 *	description:	
 *
 **/
static inline int move_safe(int x, int y, int ** maze){
	if (!is_in_bounds( x , 0 , MAZE_SIZE))
		return 0;
	if (!is_in_bounds( y, 0 , MAZE_SIZE))
		return 0;
	if ((maze[x][y] == 2)||(maze[x][y] == 3))
		return 1;
	else
		return !maze[x][y];
}


/**
 *	name: 		SOLUTION_1	
 *	author: 	William Doyle
 *	params:		1x int**, 2x const struct pos, 1x int*
 *	description:	
 *
 **/
int SOLUTION_1( int **  , const struct pos , const struct pos , int * );


/**
 *	name: 		
 *	author: 	William Doyle
 *	params:		1x int**, 2x const struct pos, 1x int*
 *	description:	
 *
 **/
int SOLUTION_2( int **  , const struct pos , const struct pos , int * );

/**
 *	name: 		
 *	author: 	William Doyle
 *	params:		1x int**, 2x const struct pos, 1x int*
 *	description:	
 *
 **/
int SOLUTION_3( int **  , const struct pos , const struct pos , int * );

/**
 *	name: 		
 *	author: 	William Doyle
 *	params:		1x int**, 2x const struct pos, 1x int*
 *	description:	
 *
 **/
int SOLUTION_4( int **  , const struct pos , const struct pos , int * );

/**
 *	name: 		
 *	author: 	William Doyle
 *	params:		1x int**, 2x const struct pos, 1x int*
 *	description:	
 *
 **/
int SOLUTION_5( int **  , const struct pos , const struct pos , int * );

/**
 *	name: 		
 *	author: 	William Doyle
 *	params:		1x int**, 2x const struct pos, 1x int*
 *	description:	
 *
 **/
int SOLUTION_6( int **  , const struct pos , const struct pos , int * );

/**
 *	name: 		
 *	author: 	William Doyle
 *	params:		1x int**, 2x const struct pos, 1x int*
 *	description:	
 *
 **/
int dijkstra( int **  , const struct pos , const struct pos , int * );

/**
 *	name: 		
 *	author: 	William Doyle
 *	params:		1x int**, 2x const struct pos, 1x int*
 *	description:	
 *
 **/
int astar( int **  , const struct pos , const struct pos , int * );
