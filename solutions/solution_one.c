/*  mazer
    Copyright (C) 2020  William Doyle

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


int SOLUTION_1( int ** maze , const struct pos start, const struct pos end, int * marr){
	maze[start.x][start.y] = 3;	// show start pos
	maze[end.x][end.y] = 3;		// show end pos
	struct pos here = {start.x, start.y};	//set self at start position
	
	while((here.x != end.x)||(here.y != end.y)){
		int move = rand() %4;
		switch (move){
			default:
			case 0:
				if (move_safe(here.x+1, here.y, maze))
					++here.x;
				break;
			case 1:
				if (move_safe(here.x, here.y+1, maze))
					++here.y;
				break;
			case 2:
				if (move_safe(here.x-1, here.y, maze))
					--here.x;
				break;
			case 3:
				if (move_safe(here.x, here.y-1, maze))
					--here.y;
				break;
		
		};
		maze[here.x][here.y] = 2;
//		fast_show_maze(maze);	
//		update_and_show_maze(maze, marr);
	};
	return 1;
}


