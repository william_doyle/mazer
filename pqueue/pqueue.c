/*  mazer
    Copyright (C) 2020  William Doyle

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "pqueue.h"
#include <float.h>
/*
	remove top element from queue
	and return its data
*/

void * __dequeue(struct pqueue * self){
	if (self->top == NULL){
		fprintf(stderr, "No value to dequeue!\n");
	}
	void * return_data = self->top->data;
	struct pq_node * to_destroy = self->top;
	self->top = self->top->next;
	if (to_destroy){
		free(to_destroy);
	}
	return return_data;
}

void __enqueue(struct pqueue * self, void * new_data){
	int placed = 0;
	struct pq_node * new_pq_node = make_pq_node(new_data);
	struct pq_node * compare_member = self->top;
	if (self->top == NULL){
		self->top = new_pq_node;
		return;
	}
	while (placed == 0){
		if (self->compare(compare_member->data, new_pq_node->data) == 0){
			new_pq_node->next = compare_member->next;
			compare_member->next = new_pq_node;
			placed = 1;
			continue;
		} else if (self->compare(compare_member->data, new_pq_node->data) == -1){
			new_pq_node->next = compare_member->next;
			if (compare_member->next != NULL){
				int next_status = self->compare(compare_member->next->data, new_pq_node->data);
				switch (next_status){
					case 0:
					case 1:
						new_pq_node->next = compare_member->next;
						compare_member->next = new_pq_node;
						placed = 1;
						break;
					case -1:
						compare_member = compare_member->next;
						break;
				}
			}else{
				compare_member->next = new_pq_node;
				placed = 1;
			}
			continue;
		} else if (self->compare(compare_member, new_pq_node) == 1 ){
			compare_member = compare_member->next;
			if (compare_member == NULL){
				fprintf(stderr, "Function \"%s\" Reported: \"Could not enqueue data: %p\"\n", __func__, new_data);
			//	goto GIVE_BETTER_NAME_TO_THIS_LABEL;
			}
			continue;
		}
		struct pq_node * temp;
		temp = self->top;
		self->top = new_pq_node;
		new_pq_node->next = temp;
		placed = 1;
	}
	return;
}

struct pq_node * make_pq_node(void * _data){
	struct pq_node * self = malloc(sizeof(struct pq_node));
	if (self == NULL){
		fprintf(stderr, "Bad Malloc in %s\n", __func__);
		abort();
	}
	self->data = _data;
	self->next = NULL;
	return self;
}

struct pqueue * make_pqueue(int(*_compare_func)(void*, void*)){
	struct pqueue * self = malloc(sizeof(struct pqueue));
	if (self == NULL){
		fprintf(stderr, "Bad Malloc in %s\n", __func__);
		abort();
	}
	self->top = NULL;
	self->dequeue = __dequeue;
	self->enqueue = __enqueue;
	self->compare = _compare_func;
	self->print_raw = __print_raw;
	return self;
}

int __int_compare(void * vint1, void * vint2){
	int * num1 = (int *)vint1;
	int * num2 = (int *)vint2;
	if (*num1 == *num2){
		return 0;
	} else if (*num1 > *num2){
		return 1;
	} else if (*num1 < *num2){
		return -1;
	}
	printf("Unexpected result in %s!\n", __func__);
	return 100;
}

int __float_compare(void * vfloat1, void * vfloat2){
	float * num1 = (float *)vfloat1;
	float * num2 = (float *)vfloat2;
	if (*num1 == *num2){
		return 0;
	} else if (*num1 > *num2){
		return 1;
	} else if (*num1 < *num2){
		return -1;
	}
	printf("Unexpected result in %s!\n", __func__);
	return 100;
}

void __print_raw(struct pqueue * self){
	for(struct pq_node * current_node = self->top; current_node != NULL; current_node = current_node->next){
		printf("%p\t%d\n", current_node->data, *(int*)current_node->data);
	}
}

void destroy_pqueue(struct pqueue * self){
	if (self == NULL){
		return; // nothing to destroy
	}
	struct pq_node * current_node = self->top;
	while (current_node != NULL){
		struct pq_node * temp = current_node;
		current_node = current_node->next;
		free(temp);
	}	
	free(self);
}
