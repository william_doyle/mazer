/*  mazer
    Copyright (C) 2020  William Doyle

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#pragma once
#include "../solve_maze.h"
#include <stdbool.h>
#include <glib.h>
#include <math.h>
#include <limits.h>
#include "../pqueue/pqueue.h"
#include "../stack/stack.h"

#define _INF INT_MAX
#define INF INT_MAX/2

struct dij_node {
	unsigned x;
	unsigned y;
	bool visited;
	long tent_dist;

	struct dij_node * prev; // ptr to prev node.. if not null this node must be part of the solution
};

static inline void set_x_y(struct dij_node * this, long x, long y){
	this->x = x;
	this->y = y;
}

static inline struct dij_node * make_dij_node(int x, int y, bool visited, int tent_dist, struct dij_node * prev){
	struct dij_node * temp;
	temp = malloc(sizeof(struct dij_node));
	check_malloc(temp, __func__);
	set_x_y(temp, x, y);
	temp->visited = visited;
	temp->tent_dist = tent_dist;
	temp->prev = prev;
	return temp;
}


static inline bool pos_not_a_wall(unsigned i, unsigned k, int ** maze){
	if ((maze[i][k] == 2)||(maze[i][k] == 3)){
		return true;
	}
	return !maze[i][k]; 
}

static inline int get_distance(struct dij_node * n1, struct dij_node * n2){
	if ((n1->x == n2->x)&&(n1->y == n2->y)){
		return 0;
	}else if (n1->x == n2->x){
		return n1->y - n2->y;
		//return abs(n1->y - n2->y);
	}else if (n1->y == n2->y){
		return n1->x - n2->x;
		//return abs(n1->x - n2->x);
	}else {
		return _INF;
	}
}

static inline bool _is_in_bounds(int x, int y){
	if (x < 0){
		return false;
	} 
	if (x > MAZE_SIZE){
		return false;
	}
	if (y < 0){
		return false;
	}
	if (y > MAZE_SIZE){
		return false;
	}
	return true;
}


static inline int option_count(unsigned i, unsigned k, int ** maze){
	int count = 0;
	if (((_is_in_bounds(i+1, k))&&(pos_not_a_wall(i+1, k, maze)))||((_is_in_bounds(i-1, k))&&(pos_not_a_wall(i-1,k, maze)))){
		++count;
	}
	if (((_is_in_bounds(i, k+1))&&(pos_not_a_wall(i,k+1,maze)))||((_is_in_bounds(i, k-1))&&(pos_not_a_wall(i,k-1,maze)))){
		++count;
	}
	return count;
}


static inline int get_displacment(struct dij_node * n1, struct dij_node *n2){
	int rval;
	if (n1->y == n2->y){
		if (n2->x > n1->x){
			rval = n2->x - n1->x;
		} else {
			rval = n1->x - n2->x;
		}
		return rval;
	} else if (n1->x == n2->x){
		if (n2->y > n1->y){
			rval = n2->y - n1->y;
		} else {
			rval = n1->y - n2->y;
		}
		return rval;
	} 
	return 50;
}

int compare_dij_nodes(void * , void * );


static inline bool line_of_sight(struct dij_node * node_a, struct dij_node * node_b, int ** maze){
	// return true if the nodes have unblocked view of eachother
	if (node_a->x == node_b->x){
		if (node_a->y > node_b->y){
			// walk up the maze (from b to a)
			// if you hit a wall return false
			// if you reach node_a-> y return true
			int i;
			for (i = node_b->y; i != node_a->y; i++){
				if (maze[node_a->x][i] == 1) { // hit wall
					return false;
				}
			}
			if (i == node_a->y){
				return true;
			}
			return false;
		} else {
			// walk down the maze (still from b to a )
			// if you hit a wall return false
			// if you reach node_a->y return true
			int i;
			for (i = node_b->y; i != node_a->y; i--){
			//	for (i = node_b->y; i != node_a->y; i++){
				if (maze[node_a->x][i] == 1) { // hit wall
					return false;
				}
				
			}
			if (i == node_a->y){
				return true;
			}
			return false;
		}
	} else if (node_a->y == node_b->y){
		if (node_a->x > node_b->x){
			// walk right through the maze (from b to a)
			// if you hit a wall return false
			// if you reach node_a-> x return true
			int i;
			for (i = node_b->x; i != node_a->x; i++){
				if (maze[i][node_a->y] == 1) { // hit wall
					return false;
				}
			}
			if (i == node_a->x){
				return true;
			}
			return false;
		} else {
			// walk left the maze (still from b to a )
			// if you hit a wall return false
			// if you reach node_a->x return true
			int i;
			for (i = node_b->x; i != node_a->x; i--){
				if (maze[i][node_a->y] == 1) { // hit wall
					return false;
				}
			}
			if (i == node_a->x){
				return true;
			}
			return false;
		}
	} else {	
		return false;
	}
}

static inline void force_missed_nodes_back_to_green(int maze_size, int ** maze) {
	//traverse the list of nodes and ensure all are green
	for (int i = 0; i < maze_size; i++){					// block ensures cells with value 4 or 5 get set back to 3
		for (int k = 0; k < maze_size; k++){				// these are nodes that were being examined when the path was found
			if ((maze[i][k] == 4) || (maze[i][k] == 5)){	
				maze[i][k] = 3;
			}
		}
	}
}

static inline void create_nodes(int maze_size, int ** maze, struct pqueue * _nodes){
	for (int i = 0; i < maze_size; i++){													// find all nodal points and create nodes at that location
		for (int k = 0; k < maze_size; k++){
			if (pos_not_a_wall(i, k, maze)){
				if ( option_count(i, k, maze) >= 2 ){
					struct dij_node * temp_node = make_dij_node(i, k, false, _INF, NULL);	// create node
					_nodes->enqueue(_nodes, (void*)temp_node);
				}
			}
		}
	}
}

static inline void connect_nodes (struct dij_node * current, int ** maze, const int __amount__) {
	if (current->prev != NULL){
		if (current->prev->x == current->x){
			// find difference in y
			if (current->prev->y > current->y){
				for (int i = current->y; i <= current->prev->y; i++){
#ifdef SLOW_DIJ
					usleep(__amount__);
#endif
					maze[current->x][i] = 2;
				}
			} else if (current->prev->y < current->y) {
				for (int i = current->prev->y; i <= current->y; i++){
					maze[current->x][i] = 2;
#ifdef SLOW_DIJ
					usleep(__amount__);
#endif
				}
			}
		} else if (current->prev->y == current->y) {
			if (current->prev->x > current->x){
				for (int i = current->x; i <= current->prev->x; i++){
#ifdef SLOW_DIJ
					usleep(__amount__);
#endif
					maze[i][current->y] = 2;
				}
			} else if (current->prev->x < current->x) {
				for (int i = current->prev->x; i <= current->x; i++){
#ifdef SLOW_DIJ
					usleep(__amount__);
#endif
					maze[i][current->y] = 2;
				}
			}
		}
	}
}

// consider rethinking this function
static inline void group_push(struct dij_node * best_north, struct dij_node * best_east, struct dij_node * best_south, struct dij_node * best_west, struct stack * uvis_nebor){
	if (best_north != NULL ){
		uvis_nebor->push(uvis_nebor, best_north);
	}
	if (best_east != NULL ){
		uvis_nebor->push(uvis_nebor, best_east);
	}
	if (best_south != NULL ){
		uvis_nebor->push(uvis_nebor, best_south);
	}
	if (best_west != NULL ){
		uvis_nebor->push(uvis_nebor, best_west);
	}
}

static inline void prepare_and_display_nodes( struct pqueue * _nodes  , struct dij_node * start_node, int ** maze, struct stack * unvisited_set ) {

	for (struct pq_node* i = _nodes->top; i != NULL; i = i->next){								// display the nodes
		if ( ((struct dij_node *)i->data) == start_node){
			continue;
		}
		maze[(*(struct dij_node *)i->data).x][(*(struct dij_node *)i->data).y] = 3;
		(*(struct dij_node *)i->data).visited = false;											// set visited
		(*(struct dij_node *)i->data).tent_dist = INF;											// set tent_dist to infinity for all but start node
		unvisited_set->push(unvisited_set, (void*)((struct dij_node *)i->data));				// create set of all unvisited called the unvisited set
	}

}

static inline bool is_closer_neighbour_than(struct dij_node * current,   struct dij_node * best__, struct pq_node * i , int ** maze, char cardinal){
	switch (cardinal){
		case 'N':
			if ((best__ == NULL) || (best__->y > (*(struct dij_node *)i->data).y  ) ) {		// is better neighbour than best__
				if (line_of_sight((struct dij_node *)i->data, current, maze)) {
					return true;
				}
			}
			break;
		case 'E':
			if ((best__ == NULL) || (best__->x > (*(struct dij_node *)i->data).x  ) ) {		// is better neighbour than best__
				if (line_of_sight((struct dij_node *)i->data, current, maze)) {
					return true;
				}
			}	
			break;

		case 'S':
			if ((best__ == NULL) || (best__->y < (*(struct dij_node *)i->data).y  ) ) {		// is better neighbour than best__
				if (line_of_sight((struct dij_node *)i->data, current, maze)) {
					return true;
				}
			}	
			break;
		case 'W':
			if ((best__ == NULL) || (best__->x < (*(struct dij_node *)i->data).x  ) ) {		// is better neighbour than best__
				if (line_of_sight((struct dij_node *)i->data, current, maze)) {
					return true;
				}
			}	
			break;
		default:
			fprintf(stderr, "Please review options for the function %s\n", __func__);
			break;
	};
	return false;
}

static inline void trace_best_path(struct dij_node * current, struct dij_node * end_node, int ** maze, const int __amount__){
	for (current = end_node; current != NULL; current = current->prev){ // start at end work backword to start
		maze[current->x][current->y] = 2;
		connect_nodes(current, maze, __amount__);
	}
}


static inline void remove_node_from_set(struct stack * unvisited_set, struct dij_node * current) {
		struct stack * temp_stack = make_stack();
		struct dij_node * temp_node;
		temp_node = unvisited_set->pop(unvisited_set);
		while( temp_node != NULL){
			if (temp_node == current){
				break;
			}
			temp_stack->push(temp_stack, temp_node) ;
			temp_node = unvisited_set->pop(unvisited_set);
		}
		temp_node = temp_stack->pop(temp_stack);
		while(temp_node != NULL){
			unvisited_set->push(unvisited_set, temp_node);
			temp_node = temp_stack->pop(temp_stack);
		}
		destroy_stack(temp_stack);
}
