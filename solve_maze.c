/*  mazer
    Copyright (C) 2020  William Doyle

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "solve_maze.h"
/*
	use parse_maze.c to get a maze into a 2d array
	use logic to solve the maze
*/
//	#define DEBUG

int solve_maze(int * marr){

	show_maze(marr);
	int (*solve)(int ** , const struct pos, const struct pos, int * marr);
//	solve = SOLUTION_1;
//	solve = SOLUTION_2;
//	solve = SOLUTION_3;
//	solve = SOLUTION_4;	// working
//	solve = dijkstra;
	solve = astar;
//	solve = SOLUTION_5;
//	solve = SOLUTION_6;

	const struct pos posStart = {ENTER, 0};			// accounts for png libarary rotating image
	const struct pos posEnd = {EXIT, MAZE_SIZE-1};	// accounts for png libarary rotating image

	int **maze;
	maze = malloc(MAZE_SIZE*sizeof(int*));

	if (maze == NULL)
		return 0;

	for (int i = 0; i < MAZE_SIZE; i++)
		maze[i] = malloc(sizeof(int)*MAZE_SIZE);

	// place fake 2d array into true 2d array 
	place_single_ptr_2d_array_into_dual_ptr_2d_array(marr, maze, MAZE_SIZE);
	
	//run solution
#ifdef TIME_MAZE
	struct timer * _timer;
	new_timer(&_timer, "SOLVE_TIMES.txt");
	if (_timer == NULL){
		return EXIT_FAILURE;
	}
	_timer->start_timer(_timer);
#endif
	int is_solved = solve(maze, posStart, posEnd, marr);
#ifdef TIME_MAZE
	_timer->stop_timer(_timer);
	_timer->save_timer(_timer);
	destroy_timer(_timer);
#endif

	// place 2d array back into single ptr 2d array 
	place_dual_ptr_2d_array_into_single_ptr_2d_array(maze, marr, MAZE_SIZE);
	
	for (int i = 0; i < MAZE_SIZE; i++)
		if (maze[i] != NULL)
			free(maze[i]);

//	if (maze != NULL)
//		free(maze);

	show_maze(marr);
	return is_solved;
}
