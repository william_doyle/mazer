/*  mazer
    Copyright (C) 2020  William Doyle

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


#include "maze.h"
#include <stdbool.h>
#include <time.h>
#include <unistd.h>
#include "gpl2_notice_function.h"

#define UNSOLVED_FILE	"problem.png"
#define SOLVED_FILE	"solution.png"


//#define USE_OLD_MAZE	
//#define FORKED_MODE
int main (int argc, char ** argv){
	if (argc > 1){
		if (strcmp(argv[1], "-i") == 0) {
			printf("%s\n",png_get_copyright(NULL));
			gpl2_notice_function("Mazer", "1.0", "William Doyle", "2020");
		}	
	}
	char * solved_file_name;
	char * unsolved_file_name;
#ifdef FORKED_MODE
	int pid = fork();
	if (0 == pid){
		srand(time(NULL)*(pid+1));
		solved_file_name = "f_solution.png";
		unsolved_file_name = "f_problem.png";
	}
	else {
		solved_file_name = SOLVED_FILE;
		unsolved_file_name = UNSOLVED_FILE; 
		srand(time(NULL)*(pid+1));
	}
#endif
#ifndef FORKED_MODE
		srand(time(NULL));
		solved_file_name = SOLVED_FILE;
		unsolved_file_name = UNSOLVED_FILE; 
#endif
	int * marr_mk;		//maze to make
	int * marr_slv;		//maze to solve

//	srand(0xC0FFEE);	
	// setup ncurses
#ifdef NCUR
	setlocale(P_ALL, "");
	initscr();
	cbreak();
	noecho();
	start_color();
	if (has_colors()){// if terminal has color ability set up our colors
		init_pair(BLACK_PAIR, COLOR_BLACK, COLOR_BLACK);
		init_pair(WHITE_PAIR, COLOR_WHITE, COLOR_WHITE);
		init_pair(RED_PAIR, COLOR_RED, COLOR_RED);
		init_pair(GREEN_PAIR, COLOR_GREEN, COLOR_GREEN);
		init_pair(CURRENT_CELL_PAIR, COLOR_RED, COLOR_GREEN);
		init_pair(SHOW_NEI_PAIR, COLOR_BLUE, COLOR_GREEN);
		
		wbkgd(stdscr, COLOR_PAIR(WHITE_PAIR));
	}

	wmove(stdscr, 0, 0);
	refresh();
#endif
	marr_mk = malloc(sizeof(int) * MAZE_SIZE * MAZE_SIZE );
	marr_slv = malloc(sizeof(int) * MAZE_SIZE * MAZE_SIZE );

#ifdef USE_OLD_MAZE	
	goto SOLVE_OLD;
#endif
	if( make_maze(marr_mk) == false){
		fprintf(stderr, "failed to make maze\n");
		goto END;
	}
	if ( save_maze(marr_mk, unsolved_file_name) == false ){
		fprintf(stderr, "failed to save maze\n");
		goto END;
	}
SOLVE_OLD:
	if ( parse_maze(marr_slv, unsolved_file_name) == false ){
		fprintf(stderr, "failed to parse maze\n");
		goto END;
	}
	if ( solve_maze(marr_slv) == false ){
		fprintf(stderr, "failed to solve maze\n");
		goto END;
	}
	if ( save_solved(marr_slv, solved_file_name) == false ){
		fprintf(stderr, "failed to save solution\n");
		goto END;
	}

END:
	if (marr_mk != NULL){
		free(marr_mk);
	}
	if (marr_slv != NULL){
		free(marr_slv);
	}
#ifdef NCUR
	echo();
	char waiter;
	scanf("%c", &waiter);
	endwin();
#endif
	printf("About to reach end!\n");
	return EXIT_SUCCESS;
}


