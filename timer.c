/*  mazer
    Copyright (C) 2020  William Doyle

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "timer.h"

clock_t SHOW_DIFF (struct timer * self){
	return self->finish - self->start;
}

void START_TIMER(struct timer * self){
	self->start = clock();
}

void STOP_TIMER(struct timer * self){
	self->finish = clock();
}


void SAVE_TIMER(struct timer * self){
	FILE * fp = fopen(self->filename, "a");
	time_t now;
	time(&now);
	fprintf(fp, "OPERATION TIME: %ld, FILE CREATION: %s\n", self->show_diff(self), ctime(&now));  
	fclose(fp);
}

void new_timer(struct timer ** _self, const char * fname){
	struct timer * self = malloc(sizeof(struct timer));
	check_malloc(self, __func__);
	strcpy(self->filename , fname);
	self->show_diff = &SHOW_DIFF;
	self->start_timer = &START_TIMER;
	self->stop_timer = &STOP_TIMER;
	self->save_timer = &SAVE_TIMER;
	*_self = self;
}

void destroy_timer(struct timer * self){
	if(self)
		free(self);
}
