/*  mazer
    Copyright (C) 2020  William Doyle

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "../solve_maze.h"
#include <stdbool.h>

int SOLUTION_1( int ** maze , const struct pos start, const struct pos end, int * marr){
	maze[start.x][start.y] = 3;	// show start pos
	maze[end.x][end.y] = 3;		// show end pos
	struct pos here = {start.x, start.y};	//set self at start position
	
	while((here.x != end.x)||(here.y != end.y)){
		int move = rand() %4;
		switch (move){
			default:
			case 0:
				if (move_safe(here.x+1, here.y, maze))
					++here.x;
				break;
			case 1:
				if (move_safe(here.x, here.y+1, maze))
					++here.y;
				break;
			case 2:
				if (move_safe(here.x-1, here.y, maze))
					--here.x;
				break;
			case 3:
				if (move_safe(here.x, here.y-1, maze))
					--here.y;
				break;
		
		};
		maze[here.x][here.y] = 2;
//		fast_show_maze(maze);	
//		update_and_show_maze(maze, marr);
	};
	return 1;
}


static inline int attempt_move(struct pos * here, int the_move, int ** maze){
	/* try the move return true upon succtee */
	switch (the_move){
		default:
		case 0:
			if (move_safe(here->x+1, here->y, maze)){
				++here->x;
				return 1;
			}else return 0;
		case 1:
			if (move_safe(here->x, here->y+1, maze)){
				++here->y;
				return 1;
			}else return 0;
		case 2:
			if (move_safe(here->x-1, here->y, maze)){
				--here->x;
				return 1;
			}else return 0;
		case 3:
			if (move_safe(here->x, here->y-1, maze)){
				--here->y;
				return 1;
			}else return 0;
	};
}

int SOLUTION_2( int ** maze , const struct pos start, const struct pos end, int * marr){
	maze[start.x][start.y] = 3;	// show start pos
	maze[end.x][end.y] = 3;		// show end pos
	struct pos here = {start.x, start.y};	//set self at start position

	
	int move = rand()%4;
	while((here.x != end.x)||(here.y != end.y)){
		move = rand()%4;
		while(attempt_move(&here, move, maze)){
			
		//	attempt_move(&here, M_LEFT, maze);
			maze[here.x][here.y] = 2;
			fast_show_maze(maze);

		};


	/*	int right_dist;
		int selected_dist;

		for(int i = 0; ; i++){
			if (maze[here.x+i][here.y])
				break;
			right_dist++;
		}

		int left_dist;
		for (int i = 0; ;i++){
			if (maze[here.x-i][here.y])
				break;
			left_dist++;
		}

		int up_dist;
		for(int i = 0; ; i++){
			if (maze[here.x][here.y+i])
				break;
			up_dist++;
		}

		int down_dist;
		for (int i = 0;; i++){
			if (maze[here.x][here.y-i])
				break;
			down_dist++;
		}
//////////////////////////////////////////////////////
		if (right_dist > left_dist){
			move = M_RIGHT;
			selected_dist = right_dist;
		}
		else {
			move = M_LEFT;
			selected_dist = left_dist;
		}
		if (up_dist > selected_dist){
			move = M_UP;
			selected_dist = up_dist;
		}
		else if (down_dist > selected_dist){
			move = M_DOWN;
		};
		attempt_move(&here, move, maze);
		maze[here.x][here.y] = 2;
		*/
	//	fast_show_maze(maze);
	};
	

	return 1;
}


#define M_UP	1
#define M_DOWN	3
#define M_LEFT	2
#define M_RIGHT 0

static inline void probe(int * forward, int * back, int * right, int * left, struct pos here, int ** maze){
	struct pos probe = here;
	//feel forward
	*forward = attempt_move(&probe, M_UP, maze);
	//feel back
	probe = here; // reset
	*back = attempt_move(&probe, M_DOWN, maze);
	//feel left
	probe = here; // reset
	*left = attempt_move(&probe, M_LEFT, maze);
	//feel right
	probe = here; // reset
	*right = attempt_move(&probe, M_RIGHT, maze);
}

int SOLUTION_3( int ** maze , const struct pos start, const struct pos end, int * marr){
	maze[start.x][start.y] = 3;	// show start pos
	maze[end.x][end.y] = 3;		// show end pos
	struct pos here = {start.x, start.y};	//set self at start position
	FILE * fp;

	int forward;
	int back;
	int left;
	int right;

//	int got_stuck = 0;	
	while((here.x != end.x)||(here.y != end.y)){
		maze[here.x][here.y] = 2;
		fast_show_maze(maze);
		
		fp = fopen ("log.txt", "a");

		probe(&forward, &back, &right, &left, here, maze);

		


		






/*		if ((right)&&(got_stuck == 0)){
			attempt_move(&here, M_RIGHT, maze);
		} else if ((forward)&&(got_stuck == 0)){
			attempt_move(&here, M_UP, maze);
		} else if (left){
			attempt_move(&here, M_LEFT, maze);
		} else if (back){
			attempt_move(&here, M_DOWN, maze);
			got_stuck++;
		}

		got_stuck = (got_stuck > 1)? 0:got_stuck;
*/
		fprintf( fp, "%d %d\n", here.x, here.y);

		fclose(fp);

	}
	return 1;
	
}


struct path_node {
	int x;
	int y;
	struct path_node * next;
};

struct path_node * new_path_node ( int _x, int _y){
	struct path_node * new_node; 
	new_node = malloc(sizeof(struct path_node));
	if (check_malloc(new_node, __func__) == 0){
		exit(EXIT_FAILURE);
	}
	new_node->next = NULL;
	new_node->x = _x;
	new_node->y = _y;
	return new_node;
}

void destroy_path(struct path_node * head){
	if (head) { // ensure we never dereference null
		if (head->next == NULL){
			free(head);
			return;
		} else {
			destroy_path(head->next);
			free(head);
		}
	}
}

bool trim_path(struct path_node * top, struct path_node * lnode){
	if ((top)&&(lnode)){// check that the pointers we dereference are never null
		if ((top->x == lnode->x)&&(top->y == lnode->y)){
			// we must trim
			struct path_node * temp = malloc(sizeof(struct path_node));
			check_malloc(temp, __func__);
			*temp = *top;
			top = lnode;
			destroy_path(temp->next);
			return true;
		}
		else if (top->next == NULL){
			return false;
		} else {
			return trim_path(top->next, lnode);
		}
	}
	return false;
}

void draw_path(struct path_node * top, int ** maze){
	if (top){
		maze[top->x][top->y] = 2; // sigsegv
		if (top->next != NULL){
			draw_path(top->next, maze);//sigsegv
		}
	}
	return;
}

void append_node(struct path_node * head, struct path_node * toappend){
	if ((head)&&(toappend)){
		while(head->next != NULL)head = head->next;
		head->next = toappend;
	}	
}

int SOLUTION_4( int ** maze , const struct pos start, const struct pos end, int * marr){
/*		SOLUTION_4
 *			Use a double linked list to reduce the path length every time the path touches itself.
 *			Movement will still be random but the presented solution will still be better. 
 *		William Doyle
 *		Febuary 6th 2020
 *
 *		IDEA from feb 7th 2020: Make 2 paths (one starts ata start and one starts at end) and stop solving when they touch.
 *		ANOTHER IDEA from feb 7th 2020: Do this thing in a bunch of threads. When solved end all threads. 
 * */

	maze[start.x][start.y] = 3;	// show start pos
	maze[end.x][end.y] = 3;		// show end pos
	struct pos here = {start.x, start.y};	//set self at start position

	struct path_node * path = new_path_node(start.x, start.y);
	
	while((here.x != end.x)||(here.y != end.y)){
		int move = rand() %4;
		switch (move){
			default:
			case 0:
				if (move_safe(here.x+1, here.y, maze))
					++here.x;
				break;
			case 1:
				if (move_safe(here.x, here.y+1, maze))
					++here.y;
				break;
			case 2:
				if (move_safe(here.x-1, here.y, maze))
					--here.x;
				break;
			case 3:
				if (move_safe(here.x, here.y-1, maze))
					--here.y;
				break;
		
		};// close switch
		
		// create a node at this location
		struct path_node * local_node = new_path_node(here.x, here.y);
		if (check_malloc(local_node, __func__) == 0){
			exit(EXIT_FAILURE);
		}
	
		//traverse path
		if (trim_path(path, local_node) == false){
			append_node(path, local_node);
		}
	}// close while
	
#ifdef NCUR
	clear();
#endif
	// visit all nodes and make them as part of the path
	draw_path(path, maze);
	destroy_path(path);
	fast_show_maze(maze);
	return 1;
}


/*

bool MAZE_SOLVED = false;

void * SOLUTION_5_THREAD( void *);

struct arguments {
	int ** maze;
	struct pos start;
	struct pos end;
	int * fds;
};

int SOLUTION_5( int ** maze , const struct pos start, const struct pos end, int * marr){
	// launch a bunch of threads 
#define NUM_THRDS 1
	pthread_t tid[NUM_THRDS];
	int fds[NUM_THRDS];

	struct arguments * passer = malloc(sizeof (struct arguments));
	passer->maze = maze;
	passer->start = start;
	passer->end = end;
	passer->fds = fds;

	for (int i = 0 ; i < NUM_THRDS; i++)
		pthread_create(&tid[i], NULL, &SOLUTION_5_THREAD, (void *)passer); 
	
	for (int i = 0 ; i < NUM_THRDS; i++){
		pthread_t t_id;
		read(fds[0], &t_id, sizeof(t_id));
		pthread_join(t_id, NULL);
	}
	pthread_exit(0);	
	free(passer);

}

void * SOLUTION_5_THREAD( void * args){
//*		SOLUTION_5
 //*		ANOTHER IDEA from feb 7th 2020: Do this thing in a bunch of threads. When solved end all threads. 
// *		William Doyle
// *		Febuary 7th 2020
// * 
  
	extern bool MAZE_SOLVED;


	// extract args and make the stuff we need;
	struct arguments * catcher = args;
	int ** maze = catcher->maze;
	struct pos start = catcher->start;
	struct pos end = catcher->end;
	int * fds = catcher->fds;

	maze[start.x][start.y] = 3;	// show start pos
	maze[end.x][end.y] = 3;		// show end pos
	struct pos here = {start.x, start.y};	//set self at start position

	struct path_node * path = new_path_node(start.x, start.y);
	
	while((here.x != end.x)||(here.y != end.y)){
		int move = rand() %4;

		if (MAZE_SOLVED){ // another thread solved so we must exit;
			destroy_path(path);
			pthread_t t = pthread_self();
			write(fds[1], &t, sizeof(t));
			return NULL;
		}

		switch (move){
			default:
			case 0:
				if (move_safe(here.x+1, here.y, maze))
					++here.x;
				break;
			case 1:
				if (move_safe(here.x, here.y+1, maze))
					++here.y;
				break;
			case 2:
				if (move_safe(here.x-1, here.y, maze))
					--here.x;
				break;
			case 3:
				if (move_safe(here.x, here.y-1, maze))
					--here.y;
				break;
		
		};// close switch
		
		// create a node at this location
		struct path_node * local_node = new_path_node(here.x, here.y);
		if (check_malloc(local_node, __func__) == 0){
			exit(EXIT_FAILURE);
		}
	
		//traverse path
		if (trim_path(path, local_node) == false){
			append_node(path, local_node);
		}
	}// close while

	if (MAZE_SOLVED){ // check to be safe
		destroy_path(path);
		pthread_t t = pthread_self();
		write(fds[1], &t, sizeof(t));
		return NULL;
	}
	MAZE_SOLVED = true;// we solved so we tell everyone else to exit
	
#ifdef NCUR
	clear();
#endif
	// visit all nodes and make them as part of the path
	draw_path(path, maze);
	destroy_path(path);
//	fast_show_maze(maze);
	return NULL;
}
*/

int SOLUTION_6( int ** maze , const struct pos start, const struct pos end, int * marr){
/*		SOLUTION_6
 *		dd	4 was good. It solves the mazes. But it is kinda bad in that it could potentually go on forever. We now need to
 *			do SOLOTION_5... which will be less random and more fast (hopefully)	
 *		William Doyle
 *		Febuary 7th 2020
 * */

	maze[start.x][start.y] = 3;	// show start pos
	maze[end.x][end.y] = 3;		// show end pos
	struct pos here = {start.x, start.y};	//set self at start position

	struct path_node * path = new_path_node(start.x, start.y);
	
	while((here.x != end.x)||(here.y != end.y)){
		int move = rand() %4;
		switch (move){
			default:
			case 0:
				if (move_safe(here.x+1, here.y, maze))
					++here.x;
				break;
			case 1:
				if (move_safe(here.x, here.y+1, maze))
					++here.y;
				break;
			case 2:
				if (move_safe(here.x-1, here.y, maze))
					--here.x;
				break;
			case 3:
				if (move_safe(here.x, here.y-1, maze))
					--here.y;
				break;
		
		};// close switch
		
		// create a node at this location
		struct path_node * local_node = new_path_node(here.x, here.y);
		if (check_malloc(local_node, __func__) == 0){
			exit(EXIT_FAILURE);
		}
	
		//traverse path
		if (trim_path(path, local_node) == false){
			append_node(path, local_node);
		}
	}// close while
	
#ifdef NCUR
	clear();
#endif
	// visit all nodes and make them as part of the path
	draw_path(path, maze);
	destroy_path(path);
//	fast_show_maze(maze);
	return 1;
}
