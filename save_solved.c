/*  mazer
    Copyright (C) 2020  William Doyle

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "maze.h"
//		#define DEBUG

int save_solved(int * marr,  char * filename){
	PNG image;
	create_png(&image, MAZE_SIZE, MAZE_SIZE, PNG_COLOR_TYPE_RGB);

	for (int i = 0; i < MAZE_SIZE; i++){
		for (int k = 0; k < MAZE_SIZE*3; k+=3){
			if (*(marr+i*MAZE_SIZE+(k/3)) == 2)
				set_color(&image, 255, 0, 0);
			else if (*(marr+i*MAZE_SIZE+(k/3)) == 3)
				set_color(&image, 0, 255, 0);
			else if (*(marr+i*MAZE_SIZE+(k/3)))
				set_color(&image, 5, 5, 5);
			else
				set_color(&image, 255, 255, 255);

			put_pixel(&image, i, k/3);
			put_pixel(&image, i, (k+1)/3);
			put_pixel(&image, i, (k+2)/3);
		}
	}

	write_png(&image, filename);
/*
	PIX * _image;							//create PIX object
       	_image  = pixRead(filename);					//load the new  png file into pix object (ew reading what we just wrote)
	_image = pixRotate90(_image, 1);							//rotate 90 degrees
	_image = pixRotate90(_image, 1);							//rotate 90 degrees again	--bad style 
	_image = pixRotate90(_image, 1);							//rotate 90 degrees once more   --bad style


	pixWrite(filename, _image, 1);		
*/


	clear_png(&image);
#ifdef DEBUG
	printf("SOLUTION FILE SAVED");
#endif
	return 1;
}
