/*  mazer
    Copyright (C) 2020  William Doyle

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#pragma once
#include <string.h>
#include <stdio.h>

static inline void gpl2_notice_function (const char * program_name, const char * version_number_string, const char * author_name, const char * year_string){
	// print notices, licenses, and anything else you thing the user might want to know
	const char * gpl2_no_warranty = "This program is distributed in the hope that it will be useful,\nbut WITHOUT ANY WARRANTY; without even the implied warranty of\nMERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\nGNU General Public License for more details.";
	const char * gpl2_freedom = "This program is free software; you can redistribute it and/or\nmodify it under the terms of the GNU General Public License\nas published by the Free Software Foundation; either version 2\nof the License, or (at your option) any later version.";
	char gpl2_notice [256];
	char command [256];
	
	sprintf(gpl2_notice, "%s version %s, Copyright (C) %s %s \n%s comes with ABSOLUTELY NO WARRANTY; for details type `show w'.\nThis is free software, and you are welcome to redistribute it \nunder certain conditions; type `show c' for details.", program_name, version_number_string, year_string, author_name, program_name );
	
	printf("%s\n", gpl2_notice);

	fgets(command, sizeof command, stdin);
	
	if (strncmp(command, "show w", 6) == 0){
		printf("\n%s\n\n", gpl2_no_warranty);
		getchar();
	}
	else if (strncmp(command, "show c", 6) == 0) {
		printf("\n%s\n\n", gpl2_freedom);
		getchar();
	}
}
