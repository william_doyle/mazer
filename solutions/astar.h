/*  mazer
    Copyright (C) 2020  William Doyle

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#pragma once
#include "dijkstra.h"

#include "../solve_maze.h"
#include <stdbool.h>
#include <glib.h>
#include <math.h>
#include <limits.h>
#include "../pqueue/pqueue.h"
#include "../stack/stack.h"


/**
 *	name: minus
 *	type: int
 *	parms: 2 int
 *
 * */
static inline int minus(int num1, int num2){
	return num1 - num2;
}


struct astar_node {
	unsigned x;
	unsigned y;
	bool visited;
	long tent_score;

	struct astar_node * prev; // ptr to prev node.. if not null this node must be part of the solution
};

static inline int closer_to_goal_bonus(struct astar_node * current, struct astar_node * cur_nei, struct astar_node * end_node){
	int x_displacment_nei = (cur_nei->x > end_node->x)? cur_nei->x - end_node->x: end_node->x - cur_nei->x;	
	int y_displacment_nei = (cur_nei->y > end_node->y)? cur_nei->y - end_node->y: end_node->y - cur_nei->y;	
	int displacment_nei = sqrt( pow(x_displacment_nei, 2) + pow(y_displacment_nei, 2));

	int x_displacment = (current->x > end_node->x)? current->x - end_node->x: end_node->x - current->x;	
	int y_displacment = (current->y > end_node->y)? current->y - end_node->y: end_node->y - current->y;	
	int displacment = sqrt( pow(x_displacment, 2) + pow(y_displacment, 2));

	if (displacment_nei < displacment){
	//	return (int)(minus(displacment, displacment_nei));
	//	return (int)(minus(displacment, displacment_nei)/2);
		return 2;
	}

	return 0;	
}

static inline void astar_set_x_y(struct astar_node * this, long x, long y){
	this->x = x;
	this->y = y;
}

struct astar_node * make_astar_node(int x, int y, bool visited, int tent_score, struct astar_node * prev){
	struct astar_node * temp;
	temp = malloc(sizeof(struct astar_node));
	check_malloc(temp, __func__);
	astar_set_x_y(temp, x, y);
	temp->visited = visited;
	temp->tent_score = tent_score;
	temp->prev = prev;
	return temp;
}

static inline void astar_prepare_and_display_nodes( struct pqueue * _nodes  , struct astar_node * start_node, int ** maze, struct stack * unvisited_set ) {
	for (struct pq_node* i = _nodes->top; i != NULL; i = i->next){								// display the nodes
		if ( ((struct astar_node *)i->data) == start_node){
			continue;
		}
		maze[(*(struct astar_node *)i->data).x][(*(struct astar_node *)i->data).y] = 3;
		(*(struct astar_node *)i->data).visited = false;											// set visited
		(*(struct astar_node *)i->data).tent_score = INF;											// set tent_score to infinity for all but start node
		unvisited_set->push(unvisited_set, (void*)((struct astar_node *)i->data));				// create set of all unvisited called the unvisited set
	}
}

static inline struct astar_node * get_node_with_smallest_tent_score(struct stack * nodes){
	struct astar_node * best_yet;
	struct astar_node * temp;
	struct stack * temp_stack = make_stack();
	best_yet = nodes->pop(nodes);
	for (temp = nodes->pop(nodes);temp != NULL;temp = nodes->pop(nodes)){
		if (temp->tent_score < best_yet->tent_score){
			temp_stack->push(temp_stack, best_yet);
			best_yet = temp;
		} else {
			temp_stack->push(temp_stack, temp);
		}
	}
	temp = temp_stack->pop(temp_stack);
	while (temp != NULL){
		nodes->push(nodes, (void*)temp);
		temp = temp_stack->pop(temp_stack);
	}
	nodes->push(nodes, (void*)best_yet);
	return best_yet;
}

static inline bool astar_line_of_sight(struct astar_node * node_a, struct astar_node * node_b, int ** maze){
	// return true if the nodes have unblocked view of eachother
	if (node_a->x == node_b->x){
		if (node_a->y > node_b->y){
			// walk up the maze (from b to a)
			// if you hit a wall return false
			// if you reach node_a-> y return true
			int i;
			for (i = node_b->y; i != node_a->y; i++){
				if (maze[node_a->x][i] == 1) { // hit wall
					return false;
				}
			}
			if (i == node_a->y){
				return true;
			}
			return false;

		} else {
			// walk down the maze (still from b to a )
			// if you hit a wall return false
			// if you reach node_a->y return true
			int i;
			for (i = node_b->y; i != node_a->y; i--){
				if (maze[node_a->x][i] == 1) { // hit wall
					return false;
				}
				
			}
			if (i == node_a->y){
				return true;
			}
			return false;
		}
	} else if (node_a->y == node_b->y){
		if (node_a->x > node_b->x){
			// walk right through the maze (from b to a)
			// if you hit a wall return false
			// if you reach node_a-> x return true
			int i;
			for (i = node_b->x; i != node_a->x; i++){
				if (maze[i][node_a->y] == 1) { // hit wall
					return false;
				}
			}
			if (i == node_a->x){
				return true;
			}
			return false;
		} else {
			// walk left the maze (still from b to a )
			// if you hit a wall return false
			// if you reach node_a->x return true
			int i;
			for (i = node_b->x; i != node_a->x; i--){
				if (maze[i][node_a->y] == 1) { // hit wall
					return false;
				}
			}
			if (i == node_a->x){
				return true;
			}
			return false;
		}
	} else {	
		return false;
	}
}

static inline bool is_better_neighbour_than(struct astar_node * current,   struct astar_node * best__, struct pq_node * i , int ** maze, char cardinal){
	switch (cardinal){
		case 'N':
			if ((best__ == NULL) || (best__->y > (*(struct astar_node *)i->data).y  ) ) {		// is better neighbour than best__
				if (astar_line_of_sight((struct astar_node *)i->data, current, maze)) {
					return true;
				}
			}
			break;
		case 'E':
			if ((best__ == NULL) || (best__->x > (*(struct astar_node *)i->data).x  ) ) {		// is better neighbour than best__
				if (astar_line_of_sight((struct astar_node *)i->data, current, maze)) {
					return true;
				}
			}	
			break;

		case 'S':
			if ((best__ == NULL) || (best__->y < (*(struct astar_node *)i->data).y  ) ) {		// is better neighbour than best__
				if (astar_line_of_sight((struct astar_node *)i->data, current, maze)) {
					return true;
				}
			}	
			break;
		case 'W':
			if ((best__ == NULL) || (best__->x < (*(struct astar_node *)i->data).x  ) ) {		// is better neighbour than best__
				if (astar_line_of_sight((struct astar_node *)i->data, current, maze)) {
					return true;
				}
			}	
			break;
		default:
			fprintf(stderr, "Please review options for the function %s\n", __func__);
			break;
	};
	return false;
}

static inline void astar_create_nodes(int maze_size, int ** maze, struct pqueue * _nodes){
	for (int i = 0; i < maze_size; i++){													// find all nodal points and create nodes at that location
		for (int k = 0; k < maze_size; k++){
			if (pos_not_a_wall(i, k, maze)){
				if ( option_count(i, k, maze) >= 2 ){
					struct astar_node * temp_node = make_astar_node(i, k, false, _INF, NULL);	// create node
					_nodes->enqueue(_nodes, (void*)temp_node);
				}
			}
		}
	}
}


static inline void astar_group_push(struct astar_node * best_north, struct astar_node * best_east, struct astar_node * best_south, struct astar_node * best_west, struct stack * uvis_nebor){
	if (best_north != NULL ){
		uvis_nebor->push(uvis_nebor, best_north);
	}
	if (best_east != NULL ){
		uvis_nebor->push(uvis_nebor, best_east);
	}
	if (best_south != NULL ){
		uvis_nebor->push(uvis_nebor, best_south);
	}
	if (best_west != NULL ){
		uvis_nebor->push(uvis_nebor, best_west);
	}
}

static inline int astar_get_displacment(struct astar_node * n1, struct astar_node *n2){
	int rval;
	if (n1->y == n2->y){
		if (n2->x > n1->x){
			rval = n2->x - n1->x;
		} else {
			rval = n1->x - n2->x;
		}
		return rval;
	} else if (n1->x == n2->x){
		if (n2->y > n1->y){
			rval = n2->y - n1->y;
		} else {
			rval = n1->y - n2->y;
		}
		return rval;
	} 
	return 50;
}

void free_astar_node(struct astar_node * to_free){
	if (to_free){
		free(to_free);
	}
}

static inline void astar_connect_nodes (struct astar_node * current, int ** maze, const int __amount__) {
	if (current->prev != NULL){
		if (current->prev->x == current->x){
			// find difference in y
			if (current->prev->y > current->y){
				for (int i = current->y; i <= current->prev->y; i++){
#ifdef SLOW_DIJ
					usleep(__amount__);
#endif
					maze[current->x][i] = 2;
				}
			} else if (current->prev->y < current->y) {
				for (int i = current->prev->y; i <= current->y; i++){
					maze[current->x][i] = 2;
#ifdef SLOW_DIJ
					usleep(__amount__);
#endif
				}
			}
		} else if (current->prev->y == current->y) {
			if (current->prev->x > current->x){
				for (int i = current->x; i <= current->prev->x; i++){
#ifdef SLOW_DIJ
					usleep(__amount__);
#endif
					maze[i][current->y] = 2;
				}
			} else if (current->prev->x < current->x) {
				for (int i = current->prev->x; i <= current->x; i++){
#ifdef SLOW_DIJ
					usleep(__amount__);
#endif
					maze[i][current->y] = 2;
				}
			}
		}
	}
}

static inline void astar_trace_best_path(struct astar_node * current, struct astar_node * end_node, int ** maze, const int __amount__){
	if (current = current->prev->prev){
	  // fprintf(stderr, "This does not work! Function broken: %s\n", __func__);
//	   return;
	}	   
	for (current = end_node; current != NULL; current = current->prev){ // start at end work backword to start
		maze[current->x][current->y] = 2;
		astar_connect_nodes(current, maze, __amount__);
	//	fprintf(stderr, "Inside: %s. Current: %p End_Node: %p Prev: %p\n", __func__, current, end_node, current->prev);
	}
}

static inline void astar_remove_node_from_set(struct stack * unvisited_set, struct astar_node * current) {
		struct stack * temp_stack = make_stack();
		struct astar_node * temp_node;
		temp_node = unvisited_set->pop(unvisited_set);
		while( temp_node != NULL){
			if (temp_node == current){
				break;
			}
			temp_stack->push(temp_stack, temp_node) ;
			temp_node = unvisited_set->pop(unvisited_set);
		}
		temp_node = temp_stack->pop(temp_stack);
		while(temp_node != NULL){
			unvisited_set->push(unvisited_set, temp_node);
			temp_node = temp_stack->pop(temp_stack);
		}
		destroy_stack(temp_stack);
}







