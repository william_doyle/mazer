 /*
	mazer
    Copyright (C) 2020  William Doyle

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <libpng/png.h>		
#include "pngassist.h"
#include <ncurses.h>
#include <locale.h>
#include <time.h>
#include <unistd.h>
#include "check_malloc.h"
#include "timer.h"
#include <string.h>
#include <malloc.h>
#include <pthread.h>
//#include <leptonica/allheaders.h>	//used to rotate png


#define MAZE_SIZE 50//86// 5000
#define FILL_CH	"\u25A0"
#define EMPTY_CH	"\u25A1"
		#define SLOW_DIJ		// show the dijkstra solution slowly 

	#define TIME_MAZE
	#define NCUR
	#define RANDOM_WALLS
	#define NO_OUT
#define ENTER	1
#define EXIT	MAZE_SIZE-2

#ifdef NCUR
	#define BLACK_PAIR 5
	#define WHITE_PAIR 6
	#define RED_PAIR 16
	#define GREEN_PAIR 17
	#define CURRENT_CELL_PAIR 18
	#define SHOW_NEI_PAIR 19

#endif

int make_maze(int *);
int save_maze(int *, char *);
int parse_maze(int * , const char *);
int solve_maze(int * );
int save_solved(int * , char *);


static inline void show_maze(const int * marr){
#ifdef NCUR
	wmove(stdscr, 0 , 0);
#endif
	for (int i = 0; i < MAZE_SIZE; i++){
		for (int k = 0; k < MAZE_SIZE; k++){
#ifdef NCUR
			if (*(marr + i*MAZE_SIZE+k) == 3){
				attron(COLOR_PAIR(GREEN_PAIR));
			}else if (*(marr + i*MAZE_SIZE+k) == 2){
				attron(COLOR_PAIR(RED_PAIR));
			}else if (*(marr + i*MAZE_SIZE+k)){
				attron(COLOR_PAIR(BLACK_PAIR));
			}else {
				attron(COLOR_PAIR(WHITE_PAIR));
			}
			refresh();
			printw("%*s", 4, (*(marr + i*MAZE_SIZE+k))? FILL_CH:EMPTY_CH );
#endif
#ifndef NCUR
			printf("%*s", 4, (*(marr + i*MAZE_SIZE+k))? FILL_CH:EMPTY_CH );
#endif
		}
#ifdef NCUR
		printw("\n");
#endif
#ifndef NCUR
		printf("\n");
#endif
	}

}


static inline void fast_show_maze( int ** marr){
#ifdef NCUR
	wmove(stdscr, 0 , 0);
#endif
	for (int i = 0; i < MAZE_SIZE; i++){
		for (int k = 0; k < MAZE_SIZE; k++){
#ifdef NCUR
			if (marr[i][k] == 3){
				attron(COLOR_PAIR(GREEN_PAIR));
			} else if (marr[i][k] == 4){
				static int prev_i = 0;
				static int prev_k = 0;
				if ((prev_i != i) || (prev_k != k)){
					if ((prev_i != 0)&&(prev_k != 0)){
						marr[prev_i][prev_k] = 3;
					}
				}
				attron(COLOR_PAIR(CURRENT_CELL_PAIR));
				prev_i = i;
				prev_k = k;
				//continue;
			} else if (marr[i][k] == 5){
				static int prev_n_i = 0;
				static int prev_n_k = 0;
				if ((prev_n_i != i) || (prev_n_k != k)) {
					if ((prev_n_i != 0)&&(prev_n_k != 0)){
						marr[prev_n_i][prev_n_k] = 3;
					}
				}
				attron(COLOR_PAIR(SHOW_NEI_PAIR));
				prev_n_i = i;
				prev_n_k = k;
				//continue;
			} else if (marr[i][k] == 2){
				attron(COLOR_PAIR(RED_PAIR));
			}else if (marr[i][k]){
				attron(COLOR_PAIR(BLACK_PAIR));
			}else {
				attron(COLOR_PAIR(WHITE_PAIR));
			}
			refresh();
			printw("%*s", 4, (marr[i][k])? FILL_CH:EMPTY_CH );
#endif
#ifndef NCUR
			printf("%*s", 4, (marr[i][k])? FILL_CH:EMPTY_CH );
#endif
		}
#ifdef NCUR
		printw("\n");
#endif
#ifndef NCUR
		printf("\n");
#endif
	}
}

/* place a single ptr (fake) 2D array into a traditional 2D array... allows me to use [][] notation instead of ptr arithmatic */
static inline void place_single_ptr_2d_array_into_dual_ptr_2d_array(int * marr, int **chamber, int sub_size){
	for (int i = 0; i < sub_size; i++)
		for (int k = 0; k < sub_size; k++)
			chamber[i][k] = *(marr+i*sub_size+k);
}

/* copy [][] notation 2D array into single ptr *(arr+i*row_size+k) addressable fake 2D array */
static inline void place_dual_ptr_2d_array_into_single_ptr_2d_array(int **chamber, int *marr, int sub_size){
	for (int i = 0; i < sub_size; i++)
		for (int k = 0; k < sub_size; k++)
			*(marr+i*sub_size+k) = chamber[i][k];
}


