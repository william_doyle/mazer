/*  mazer
    Copyright (C) 2020  William Doyle

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#pragma once
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include "check_malloc.h"
#include <string.h>

struct timer{
	clock_t start;
	clock_t finish;
	char filename[50];
	clock_t (*show_diff)(struct timer*);
	void (*start_timer)(struct timer*);
	void (*stop_timer)(struct timer*);
	void (*save_timer)(struct timer*);
};

clock_t SHOW_DIFF (struct timer * );

void START_TIMER(struct timer * );

void STOP_TIMER(struct timer * );

void SAVE_TIMER(struct timer * );

void new_timer(struct timer ** , const char *);

void destroy_timer(struct timer * );
