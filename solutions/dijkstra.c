/*  mazer
    Copyright (C) 2020  William Doyle

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "dijkstra.h"
#include "../stack/stack.h"
//#define DEBUG
#include <pthread.h>

int compare_dij_nodes(void * n1, void * n2){
	struct dij_node * dnode1 = (struct dij_node *)n1;
	struct dij_node * dnode2 = (struct dij_node *)n2;
	if (dnode1->tent_dist == dnode2->tent_dist){
		return 0;
	} else if (dnode1->tent_dist > dnode2->tent_dist){
		return 1;
	} else if (dnode1->tent_dist < dnode2->tent_dist){
		return -1;
	}
	printf("Unexpected result in %s!\n", __func__);
	return 100;
}

void set_as_tent_dist(struct dij_node * this, int * tent_dist_val){
	this->tent_dist = *tent_dist_val;
}

void free_dij_node(struct dij_node * to_free){
	if (to_free){
		free(to_free);
	}
}

void set_dist_and_prev(struct dij_node * n){
	n->tent_dist = _INF;
	n->prev = NULL;
}

int compare_dij(struct dij_node * n1, struct dij_node * n2){
	if (n1->tent_dist < n2->tent_dist)
		return 1;
	else if (n1->tent_dist > n2->tent_dist)
		return -1;
	else
		return 0;
}

static inline struct dij_node * get_node_with_smallest_tent_dist(struct stack * nodes){
	struct dij_node * best_yet;
	struct dij_node * temp;
	struct stack * temp_stack = make_stack();
	best_yet = nodes->pop(nodes);
	for (temp = nodes->pop(nodes);temp != NULL;temp = nodes->pop(nodes)){
		if (temp->tent_dist < best_yet->tent_dist){
			temp_stack->push(temp_stack, best_yet);
			best_yet = temp;
		} else {
			temp_stack->push(temp_stack, temp);
		}
	}
	temp = temp_stack->pop(temp_stack);
	while (temp != NULL){
		nodes->push(nodes, (void*)temp);
		temp = temp_stack->pop(temp_stack);
	}
	nodes->push(nodes, (void*)best_yet);
	return best_yet;
}

#ifdef SLOW_DIJ
	#define SLOW_MODE
#endif
bool quit_slow_mode = false;
void * slow_show( void * data){
#ifdef SLOW_MODE
	int ** maze = (int ** )data;
	for (;;){
//		usleep(100);
		fast_show_maze(maze);
		if (quit_slow_mode == true){
			return NULL;
		}
	}
#endif
}

int dijkstra(int ** maze, const struct pos start, const struct pos end, int * marr){
	struct pqueue * _nodes = make_pqueue(compare_dij_nodes);												
	struct dij_node * current;
	struct dij_node * start_node = make_dij_node(start.x, start.y, false, 0, NULL);
	struct dij_node * end_node = make_dij_node(end.x, end.y, false, _INF, NULL);
	struct stack * unvisited_set = make_stack();
	maze[start_node->x][start_node->y] = 3;
	maze[end_node->x][end_node->y] = 3;
	
#ifdef SLOW_DIJ
	pthread_t ptid;
	pthread_create(&ptid, NULL, slow_show, (void*)maze);
#endif
	const int __amount__ = 15000;

	_nodes->enqueue(_nodes, (void*)end_node);	
	create_nodes(MAZE_SIZE, maze, _nodes);																	// create nodes at maze junctions
	prepare_and_display_nodes(_nodes, start_node, maze, unvisited_set );									// prepare nodes and set them on maze
	start_node->tent_dist = 0;																				// set initial node tent_dist to zero

	// set the initial node as current
	for (current = start_node; current != NULL; current = get_node_with_smallest_tent_dist(unvisited_set)){
		// consider all of its unvisited neighbours
		struct stack * uvis_nebor = make_stack();
		struct dij_node * best_north = NULL;
		struct dij_node * best_east = NULL;
		struct dij_node * best_south = NULL;
		struct dij_node * best_west = NULL;

		if (current->tent_dist == INF){
			break;
		}
#ifdef SLOW_MODE
		maze[current->x][current->y] = 4;
		usleep(4000);
#endif
		for (struct pq_node* i = _nodes->top; i != NULL; i = i->next){
			// if data is a perfect best neighbour push it to the stack
			if ((*(struct dij_node *)i->data).x == current->x){			// if x's are the same
				if ((*(struct dij_node *)i->data).y > current->y ){		// if is higher north than me
					if	(is_closer_neighbour_than(current, best_north, i, maze, 'N'))
						best_north = ((struct dij_node *)i->data);
				}
				else if ((*(struct dij_node *)i->data).y < current->y ){		// if is further south than me
					if (is_closer_neighbour_than(current, best_south, i, maze, 'S'))	
						best_south = (struct dij_node *)(i->data);
				}
			}
			else if ((*(struct dij_node *)i->data).y == current->y)	{	// if y's are the same
				if ((*(struct dij_node *)i->data).x > current->x ){		// if is higher east than me
					if (is_closer_neighbour_than(current, best_east, i, maze, 'E'))	
						best_east = (struct dij_node *)(i->data);
				}
				else if ((*(struct dij_node *)i->data).x < current->x ){		// if is further west than me
					if (is_closer_neighbour_than(current, best_west, i, maze, 'W'))		
						best_west = (struct dij_node *)(i->data);
				}
			}
		}
		group_push(best_north, best_east, best_south, best_west, uvis_nebor);

		struct dij_node * cur_nei = (struct dij_node*)uvis_nebor->pop(uvis_nebor);
		while (cur_nei != NULL){
			// calculate cur_nei tent_dist through curent node
			long tent_dist_through_current;
			tent_dist_through_current = current->tent_dist + get_displacment(current, cur_nei);
#ifdef SLOW_MODE
			maze[cur_nei->x][cur_nei->y] = 5;
			usleep(4000);
#endif
			if (tent_dist_through_current < cur_nei->tent_dist){	// compair newly calculated tent_dist to current assigned value of neighbours
				cur_nei->tent_dist = tent_dist_through_current;
				cur_nei->prev = current;							// assign the smaller one to cur_nei->tent_dist		
			}
			else {
				// keep the same
				cur_nei->tent_dist = cur_nei->tent_dist;
			}
			cur_nei = (struct dij_node*)uvis_nebor->pop(uvis_nebor);
		}

		// when we are done considering all unvisited neighbours of the current node
		current->visited = true;		// mark the current node as visited
		remove_node_from_set( unvisited_set, current);	// remove the current node from the set 
		destroy_stack(uvis_nebor);
		if (end_node->visited == true ){		// if the destination node has been marked visited
			break;
		}
	}	
	
	// find path from start node to end node
#ifdef SLOW_MODE
	force_missed_nodes_back_to_green(MAZE_SIZE, maze);
#endif
	trace_best_path(current, end_node, maze, __amount__);
	
#ifdef SLOW_DIJ
	quit_slow_mode = true;
	pthread_join(ptid, NULL);
#endif

	//free malloced memory
	destroy_stack(unvisited_set);
	for (struct pq_node* i = _nodes->top; i != NULL; i = i->next){
		free_dij_node((struct dij_node *)i);
	}
	free_dij_node(start_node);
	free_dij_node(end_node);

	return 1;
}





